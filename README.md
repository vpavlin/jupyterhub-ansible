<p align="center">
    <img src="datahub_logo.png" width="400" height="105" alt="Data Hub, an AI platform powered by Open Source" title="Data Hub, an AI platform powered by Open Source" />
</p>

JupyterHub APB
=========

[![Docker Repository on Quay](https://quay.io/repository/opendatahub/jupyterhub-apb/status "Docker Repository on Quay")](https://quay.io/repository/opendatahub/jupyterhub-apb)

This APB allows you to install JupyterHub with Spark Operator which are both core part of OpenDataHub.io.

Components
=========

List of componets follows:

* Jupyterhub
* PostgreSQL
* Spark Operator (optional, on by default)
* Minio (S3 replacement for development)

JupyterHub Singleuser Server images:

* Minimal
* Scipy
* Tensorflow
* Spark Minimal (spawns Ephemeral Spark cluster)
* Spark Scipy (spawns Ephemeral Spark cluster)

How to test
=========

You can deploy this APB from our image on Quay.io - [quay.io/opendatahub/jupyterhub-apb](https://quay.io/repository/opendatahub/jupyterhub-apb) by using the [APB CLI](https://github.com/ansibleplaybookbundle/ansible-playbook-bundle/blob/master/docs/apb_cli.md) or [Ansible Service Broker](https://github.com/openshift/ansible-service-broker#getting-started-with-the-ansible-service-broker).

There are 2 plans ready for you:

* prod
* dev

The main difference is that `dev` plan will deploy [Minio](https://github.com/minio/minio) for S3 endpoint, so that you don't have to worry about setting up Ceph Object Storage or pay for AWS S3 while developing and testing things locally. The `dev` plan also deploys only one JupyterHub Singleuser Server Image (Spark Minimal) to speed up the setup time and save resources. You can also turn of Spark Operator while deploying the `dev` plan - also to save resources.

See [apb.yml](apb.yml) for the list of parameters.



